import os

def IntroducirValor(dicc, contador):
    valor = "Valor"+str(contador)
    dicc[valor] = input("Introduce un nombre:")

def EliminarValor(dicc):
    borrar = input("Introduce la clave del valor a eliminar: ")
    print("Has borrado el valor {}".format(dicc.get(borrar)))
    dicc.pop(borrar)
    os.system("pause")

def MostrarTodo(dicc):
    print("Valores del diccionario")
    cont = 0
    for k, v in dicc.items():
        print("{} -> {}".format(k, v))
        cont+=1

    if (cont==0):
        print("El diccionario esta vacio")

def BorrarTodo(dicc):
    seguro = input("¿Estas seguro? S-Si // N-No ")
    if(seguro.upper() == "S"):
        dicc.clear()
        print("Diccionario borrado.")
    else:
        print("Has elegido no borrar el diccionario.")
    os.system("pause")
