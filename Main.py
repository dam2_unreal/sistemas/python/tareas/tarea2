from Funciones import *

salida = True
dicc = dict()
contador = 0;

while(salida):
    os.system('cls')
    print("1 - Introducir un valor en el diccionaio")
    print("2 - Eliminar un nombre del diccionario")
    print("3 - Mostrar todos los valores")
    print("4 - Borrar Diccionario")
    print("9 - Salir")
    eleccion = input("Inserta un numero del menu: ")
    os.system('cls')

    if(eleccion == "1"):
        IntroducirValor(dicc, contador)
        contador+=1
    elif(eleccion == "2"):
        EliminarValor(dicc)
    elif (eleccion == "3"):
        MostrarTodo(dicc)
        os.system("pause")
    elif (eleccion == "4"):
        BorrarTodo(dicc)
    elif (eleccion == "9"):
        print("Saliendo del programa")
        print("Programa realizado por Jose Andres Sanchez Perez")
        os.system("pause")
        salida = False